package com.java.life;

import java.util.Arrays;

/**
 * @author vikram batra
 * Compares each element with the next element and swaps if not in order.
 * After each iteration, the largest number is at the right.
 * Complexity is o(n2)
 */
public class BubbleSort {

	public static void main(String[] args) {
		//array to be sorted
		int [] array = {43,4,1,42,54,7645,764,312,65,76};
		
		for (int i = 0; i < array.length ; i++) {
			//second loop runs to length-i to avoid extra iterations
			for (int j = 0; j < array.length-1-i; j++) {
				if (array[j] > array[j+1]) {
					int temp = array[j];
					array[j] = array[j+1];
					array[j+1] = temp;
				}
			}
		}
		
		System.out.println("sorted array " + Arrays.toString(array));
	}
}
