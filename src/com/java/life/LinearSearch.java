package com.java.life;

public class LinearSearch {

	/**
	 * @author vikram batra
	 */
	public static void main(String[] args) {

		// Array to be searched
		int [] array = {1,4,5,43,54,7645,764,312,65,76};
		
		// value to be searched
		int data = 3;
		boolean found = false;
		
		for (int i = 0; i < array.length; i++) {
			if (array[i] == data) {
				System.out.println("data value found at index " + i);
				found = true;
				break;
			}
			if (i== array.length-1 && !found) {
				System.out.println("data value not present");
			}
		}
		
	}

}
