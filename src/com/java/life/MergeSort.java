package com.java.life;

import java.util.Arrays;

public class MergeSort {

	public static int [] array = {43,4,1,42,54,7645,764,312,65,76};
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		mergeSort(0, array.length-1);
		
	}

	private static void mergeSort(int low, int high) {
		
		if (low < high) {
			int mid = (low + high) / 2;
			mergeSort(low, mid);
			mergeSort(mid+1, high);
			merge(low, mid, high);
		}
		System.out.println("sorted array " + Arrays.toString(array));
	}
	
	private static void merge(int low, int mid, int high) {
		int [] temp = new int [high-low+1];
		
		int i = low;
		int j = mid+1;
		int k = 0;
		
		while (i <= mid && j <=high) {
			if (array[i] < array[j]) {
				temp [k] = array [i];
				k++;
				i++;
			} else {
				temp[k] = array[j];
				k++;
				j++;
			}
		}
		while (i <= mid) {
			temp[k++] = array[i++];
		}
		while (j <= high) {
			temp[k++] = array[j++];
		}
		
		for(k = 0; k < temp.length; k++) {
			array[low+k] = temp[k];
		}
	}
}
