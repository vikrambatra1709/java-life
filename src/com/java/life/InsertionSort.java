package com.java.life;

import java.util.Arrays;

public class InsertionSort {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		int [] array = {43,4,1,42,54,7645,764,312,65,76};
		
		for(int i =1; i< array.length; i++) {
			int val = array[i];
			int j = i-1;
			while (j >= 0 && array[j] > val) {
				int temp = array[j];
				array[j] = array[j+1];
				array[j+1] = temp;
				j--;
			}
		}
		
		System.out.println("sorted array " + Arrays.toString(array));
	}

}
