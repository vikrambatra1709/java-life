package com.java.life;

public class BinarySearch {

	/**
	 * @author vikram batra
	 */
	public static void main(String[] args) {
		// array to be searched. Needs to be sorted
		int [] array = {2,4,5,43,54,67,98,109,1076};

		// value to be searched
		int data = 87;
		
		int startIndex = 0;
		int endIndex = array.length-1;

		int found = search(array, data, startIndex, endIndex);
	}
	
	private static int search(int [] array, int data, int startIndex, int endIndex) {
		int index = (endIndex + startIndex) / 2;
		System.out.println("index " + index);
		int val = array[index];
		if(startIndex < endIndex) {
			if( val == data) {
				System.out.println("found at index " + index);
				return index;
			} else if ( val > data) {
				return search(array, data, startIndex, index-1);
			} else {
				return search(array, data, index+1, endIndex);
			}
		} else if(startIndex ==  endIndex) {
			if(data == array[startIndex]) {
				System.out.println("found at index " + index);
				return index;
			}
		}
		System.out.println("not found");
		return -1;
	}

}
